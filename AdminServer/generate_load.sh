#!/bin/bash

#until [[ `curl -s -o /dev/null/ -w "%{http_code}" http://website.local/load.php\` != 200 ]];
#do
#	echo "HTTP request sent to website.local/load.php"
#	sleep 2
#done
until [[ `curl -s -o /dev/null/ -w "%{http_code}" http://192.168.1.2/index.php` != 200 ]]
do
	echo "Sending 100x100 HTTP request sent to website.local/load.php"
	siege -c 100 -b -r 100 -H 'Host: website.local' http://192.168.1.2/generate_load.php
done
